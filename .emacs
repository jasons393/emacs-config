(org-babel-load-file (concat user-emacs-directory "config.org"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(highlight-indent-guides-auto-character-face-perc 30)
 '(highlight-indent-guides-character 9475)
 '(highlight-indent-guides-method 'character)
 '(package-selected-packages
   '(treemacs-evil treemacs hlinum eglot workgroups2 writeroom-mode evil-mu4e company-wordfreq tree-sitter-langs tree-sitter flx company-quickhelp yasnippet lua-mode rust-mode marginalia undo-tree mingus rainbow-delimiters elfeed-org elfeed multi-vterm vterm beacon smooth-scrolling clippy highlight-indent-guides org-bullets centaur-tabs aggressive-indent company solaire-mode magit orderless use-package evil)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "VictorMono Nerd Font"))))
 '(font-lock-function-name-face ((t (:slant italic))))
 '(treemacs-directory-collapsed-face ((t (:inherit default))))
 '(treemacs-directory-face ((t (:inherit (default)))))
 '(treemacs-file-face ((t (:inherit default))))
 '(treemacs-git-unmodified-face ((t (:inherit default))))
 '(treemacs-root-face ((t (:inherit default :weight bold :height 1.2)))))
