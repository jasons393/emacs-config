#+TITLE: Jasons Emacs Config
#+AUTHOR: Jason Su
If you are someone who wants to use other peoples configs, this one isn't for you.
* MELPA
  First, lets initialize MELPA for plugins.
  #+BEGIN_SRC emacs-lisp
  (require 'package)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (package-initialize)
  #+END_SRC
* Appearance
  Making Emacs look less ugly.
** Icons
   I like icons so I'm going to use the all-the-icons package.
   #+BEGIN_SRC emacs-lisp
   (use-package all-the-icons
     :if (display-graphic-p))
   #+END_SRC
** Theme
   The default Emacs theme is ugly so I am installing the Doom Emacs theme pack.
   #+BEGIN_SRC emacs-lisp
   (use-package doom-themes
    :ensure t
    :config

   (setq doom-themes-enable-bold t
   doom-themes-enable-italic t)

   (load-theme 'doom-one t)

   (doom-themes-treemacs-config)
   (doom-themes-org-config))
   #+END_SRC
   Remove the menubar, toolbar, and scrollbar.
   #+BEGIN_SRC emacs-lisp
   (menu-bar-mode -1)
   (toggle-scroll-bar -1)
   (tool-bar-mode -1)
   #+END_SRC
   Solaire mode makes the background of minibuffers darker.
   #+BEGIN_SRC emacs-lisp
   (solaire-global-mode +1)
   #+END_SRC
** Modeline
   I am going to use Doom-modeline for my modeline.
   #+BEGIN_SRC emacs-lisp
   (use-package doom-modeline
     :ensure t
     :init (doom-modeline-mode 1)
     :config
     (setq doom-modeline-height 36
	   doom-modeline-bar-width 4
	   doom-modeline-icon (display-graphic-p)
	   doom-modeline-major-mode-icon t
	   doom-modeline-major-mode-color-icon t
	   doom-modeline-buffer-state-icon t
	   doom-modeline-buffer-modification-icon t
	   doom-modeline-modal-icon t)
     (set-face-attribute 'mode-line nil :family "Fira Sans" :height 160)
     (set-face-attribute 'mode-line-inactive nil :family "Fira Sans" :height 160))
   #+END_SRC
** Dashboard
   Dashboard is the startup screen for this configuration.
   #+BEGIN_SRC emacs-lisp
   (use-package dashboard
   :ensure t
   :config
   (dashboard-setup-startup-hook)
   (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))) ;; For Emacs Daemon
   ;; Configuration
   (setq dashboard-startup-banner "~/.emacs.d/banner2.txt"
	 dashboard-center-content t
	 dashboard-set-heading-icons t
	 dashboard-set-file-icons t
	 dashboard-banner-logo-title "I   Emacs"))
   #+END_SRC
** Tabs
   Centaur tabs looks nice.
   #+BEGIN_SRC emacs-lisp
   (use-package centaur-tabs
     :demand
     :config
     (centaur-tabs-headline-match)
     (setq centaur-tabs-style "bar"
	   centaur-tabs-set-bar 'over
	   centaur-tavs--buffer-show-groups t
	   centaur-tabs-set-icons t
	   centaur-tabs-gray-out-icons t
	   centaur-tabs-set-modified-marker t
	   centaur-tabs-modified-marker " "
	   centaur-tabs-close-button " "
	   centaur-tabs-show-navigation-buttons t)
     (centaur-tabs-mode 1))
   #+END_SRC
* Tools
  Things that make using Emacs easier.
** File Management
   File management in Emacs.
*** Treemacs
    File manager sidebar.
    #+BEGIN_SRC emacs-lisp
    (use-package treemacs
      :config
      (treemacs-load-theme "doom-colors")
      (use-package treemacs-evil)
      (defun treemacs-no-linum ()
	(linum-mode -1))
      (add-hook 'treemacs-mode-hook #'treemacs-no-linum))
    #+END_SRC
** Git
   Git tools for Emacs.
*** Magit
    Git interface in emacs.
    #+BEGIN_SRC emacs-lisp
    (use-package magit
    :ensure t)
    #+END_SRC
** Orderless
   Space-separated completion style.
   #+BEGIN_SRC emacs-lisp
   (use-package orderless
   :ensure t
   :custom (completion-styles '(orderless)))
   #+END_SRC
** Vertico
   Vertico is a vertical completion UI.
   #+BEGIN_SRC emacs-lisp
   (use-package vertico
   :init
   (vertico-mode))
   #+END_SRC
** Company mode
   Compand mode is a completion engine that works with lsp-mode.
   #+BEGIN_SRC emacs-lisp
   (use-package company
     :config
     (use-package company-quickhelp
       :config
       (company-quickhelp-mode))
     (use-package company-wordfreq
       :config
       (add-hook 'text-mode-hook (lambda ()
				   (setq-local company-backends '(company-wordfreq))
				   (setq-local company-transformers nil)))))
   #+END_SRC
** Work groups
   Return to a session later with workgroups2.
   #+BEGIN_SRC emacs-lisp
   (use-package workgroups2
     :config
     (setq wg-session-file "~/.emacs.d/workgroups"))
   #+END_SRC
** Indent
   Aggressive intent aggresively intents code.
   #+BEGIN_SRC emacs-lisp
   (global-aggressive-indent-mode 1) 
   #+END_SRC
   Highlight indents,
   #+BEGIN_SRC emacs-lisp
   (use-package highlight-indent-guides
   :config
   (add-hook 'prog-mode-hook 'highlight-indent-guides-mode))
   #+END_SRC
** Parenthesis
   Smartparens implements auto parenthesis in Emacs.
   #+BEGIN_SRC emacs-lisp
   (use-package smartparens
   :ensure t
   :config
   (require 'smartparens-config))
   #+END_SRC
   Rainbow-delimiters colors parenthesis based on depth.
   #+BEGIN_SRC emacs-lisp
   (use-package rainbow-delimiters
     :config
     (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))
   #+END_SRC
** Vterm
   The default terminal(s) suck in Emacs. Vterm uses libvterm and it is much more usable.
   #+BEGIN_SRC emacs-lisp
   (use-package vterm)
   #+END_SRC
** Clippy
   Remember this guy from Windows? Well here he is in Emacs, but a bit more helpful!
   #+BEGIN_SRC emacs-lisp
   (use-package clippy)
   #+END_SRC
** Marginalia
   Extra metadata for completions.
   #+BEGIN_SRC emacs-lisp
   (use-package marginalia
   :init
     (marginalia-mode))
   #+END_SRC
** Undo Tree
   #+BEGIN_SRC emacs-lisp
   (use-package undo-tree
     :init
     (global-undo-tree-mode))
   #+END_SRC
** Treesitter
   Treesitter is a language parser.
   #+BEGIN_SRC emacs-lisp
   (use-package tree-sitter
     :config
     (global-tree-sitter-mode)
     (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))
   #+END_SRC
* Keybindings
** Evil Mode
   Evil mode is a plugin for Vim keybindings in Emacs.
*** Leader keys
   #+BEGIN_SRC emacs-lisp
   (use-package evil-leader
   :ensure t
   :config
   (global-evil-leader-mode)
   (evil-leader/set-leader "<SPC>")
   (evil-leader/set-key
     ;; Buffer
     "bl" 'list-buffers
     "bm" 'buffer-menu
     "bk" 'kill-buffer

     ;; Window management
     "wv" 'split-window-vertically
     "wh" 'split-window-horizontally
     "wu" 'windmove-up
     "wd" 'windmove-down
     "wr" 'windmove-right
     "wl" 'windmove-left
     "wq" 'delete-window
     "wsh" 'shrink-window-horizontally
     "wsv" 'shrink-window
     "weh" 'enlarge-window-horizontally
     "wev" 'enlarge-window
     "wgc" 'wg-create-workgroup
     "wgo" 'wg-open-workgroup
     "wgq" 'wg-kill-workgroup

     ;; Tab navigation
     "tf" 'centaur-tabs-forward
     "tb" 'centaur-tabs-backward
     "tc" 'centaur-tabs--create-new-tab
     "tg" 'centaur-tabs-switch-group

     ;; Magit
     "go" 'magit
     "gd" 'magit-diff
     "gp" 'magit-push

     ;; Help
     "hf" 'clippy-describe-function
     "hv" 'clippy-describe-variable
     "hdf" 'describe-function
     "hdv" 'describe-variable
     "hh" 'help

     ;; Switches
     "sc" 'global-company-mode
     "sp" 'smartparens-global-mode
     "sl" 'eglot

     ;; Flyspell
     "fst" 'flyspell-mode
     "fsn" 'flyspell-goto-next-error
     "fsc" 'flyspell-correct-word-before-point

     ;; File
     "ftt" 'treemacs
     "ftd" 'treemacs-select-directory
     "ftr" 'treemacs-refresh
     "fd" 'dired
     "fr" 'recentf-open-files
     "ff" 'find-file
     "fu" 'undo-tree-visualize

     ;; Open program
     "oc" 'calculator
     "ot" 'vterm
     "of" 'elfeed))
   #+END_SRC
*** Set evil mode
   #+BEGIN_SRC emacs-lisp
   (use-package evil
   :ensure t
   :config
   (evil-mode 1)
   (setq evil-undo-system "undo-tree")
   (evil-define-key 'normal 'global "f" 'execute-extended-command))
   #+END_SRC
*** Others
    #+BEGIN_SRC emacs-lisp
    ;; Switch between splits
    (global-set-key [S-right] 'windmove-right)
    (global-set-key [S-left] 'windmove-left)
    (global-set-key [S-up] 'windmove-up)
    (global-set-key [S-down] 'windmove-down)

    ;; Enlarge/shrink windows
    (global-set-key [C-S-right] 'enlarge-window-horizontally)
    (global-set-key [C-S-left] 'shrink-window-horizontally)
    (global-set-key [C-S-up] 'enlarge-window)
    (global-set-key [C-S-down] 'shrink-window)
    #+END_SRC
** Which Key
   Which-key gives you a list of key binds.
   #+BEGIN_SRC emacs-lisp
   (use-package which-key
   :ensure t
   :config
   (which-key-mode))
   #+END_SRC
* Org Mode
  Remove indentation in SRC,
  #+BEGIN_SRC emacs-lisp
  (setq org-edit-src-content-indentation 0)
  #+END_SRC
  Org bullets to make headers look better,
  #+BEGIN_SRC emacs-lisp
  (use-package org-bullets
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
  #+END_SRC
  Org agenda directory,
  #+BEGIN_SRC emacs-lisp
  (setq org-directory "~/.org")
  #+END_SRC
* Settings
  Set font and font size.
  #+BEGIN_SRC emacs-lisp
  (custom-set-faces
   '(default ((t (:family "VictorMono Nerd Font"))))
   '(font-lock-function-name-face ((t (:slant italic)))))
  (set-face-attribute 'default nil :height 160)
  #+END_SRC
  Remove backup files (they are annoying).
  #+BEGIN_SRC emacs-lisp
  (setq make-backup-files nil)
  #+END_SRC
  Smooth scrolling,
  #+BEGIN_SRC emacs-lisp
  (use-package smooth-scrolling
  :config
  (smooth-scrolling-mode 1))
  #+END_SRC
  Beacon shines the cursor when you scroll,
  #+BEGIN_SRC emacs-lisp
  (use-package beacon
  :config
  (beacon-mode 1))
  #+END_SRC
  Go back to recent files that you edited,
  #+BEGIN_SRC emacs-lisp
  (recentf-mode 1)
  #+END_SRC
  Save recent prompt entries,
  #+BEGIN_SRC emacs-lisp
  (setq history-length 25)
  (savehist-mode 1)
  #+END_SRC
  Remember last position you set your cursor on files,
  #+BEGIN_SRC emacs-lisp
  (save-place-mode 1)
  #+END_SRC
  Turn off ui dialogs for prompts,
  #+BEGIN_SRC emacs-lisp
  (setq use-dialog-box nil)
  #+END_SRC
  Refresh buffers if modified outside of the current buffers,
  #+BEGIN_SRC emacs-lisp
  (global-auto-revert-mode 1)
  #+END_SRC
  Highlight the line the cursor is on.
  #+BEGIN_SRC emacs-lisp
  (global-hl-line-mode nil)
  #+END_SRC
  Add line number bar,
  #+BEGIN_SRC emacs-lisp
  (global-linum-mode)
  (hlinum-activate)
  #+END_SRC
* Utilities
** Elfeed
   Elfeed is a rss feed reader for Emacs.
   #+BEGIN_SRC emacs-lisp
   (use-package elfeed-org
   :config
   (elfeed-org)
   (setq rmh-elfeed-org-files (list "~/.emacs.d/elfeed.org")))
   #+END_SRC
